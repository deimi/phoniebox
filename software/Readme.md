# Setup
## Raspberry pi
Only Pi OS lite 32bit is supported at the moment by Phoniebox 3 (https://rpi-jukebox-rfid.readthedocs.io/en/latest/install.html)

```bash
sudo apt install git unattended-upgrades lm-sensors htop
```

## Audio setup for WM8960 Audio HAT
https://www.waveshare.com/wiki/WM8960_Audio_HAT

```bash
git clone https://github.com/waveshare/WM8960-Audio-HAT
cd WM8960-Audio-HAT
sudo ./install.sh
sudo reboot

# Check if driver is loaded
sudo dkms status
# Check if audio device is listed
aplay -l
# Check if speakers are working
speaker-test

# Set the audio hat as default device
echo 'ctl.!default {
    type hw
    card 1
}' | sudo tee -a /etc/asound.conf
```

## Phoniebox 3

```bash
sudo ln -s /home/panda/ /home/pi
cd; bash <(wget -qO- https://raw.githubusercontent.com/MiczFlor/RPi-Jukebox-RFID/future3/main/installation/install-jukebox.sh)
```

Copy config files
```bash
cp jukebox.yaml ~/RPi-Jukebox-RFID/shared/settings/
cp rfid.yaml ~/RPi-Jukebox-RFID/shared/settings/
cp gpio.yaml ~/RPi-Jukebox-RFID/shared/settings/
```

## Installing on-off shim
```bash
./install_onoffshim.sh
cp cleanshutd.conf /etc/cleanshutd.conf
sudo systemctl restart cleanshutd
```
Copy `cleanshutd.conf`

## Tweaks for faster boot

```bash
sudo update-rc.d exim4 remove

sudo apt -y install dropbear
sudo sed -i 's/NO_START=1/NO_START=0/g' /etc/default/dropbear
sudo systemctl enable dropbear && sudo systemctl disable sshd && sudo reboot
```

# Spotify connect

Using raspotify (https://github.com/dtcooper/raspotify/) to enable spotify connect on the phoniebox.

https://github.com/dtcooper/raspotify/wiki/Basic-Setup-Guide

```bash
sudo apt-get -y install curl && curl -sL https://dtcooper.github.io/raspotify/install.sh | sh
```

Copy config files
```bash
cp raspotify.conf /etc/raspotity/conf
```

## Downside
- Spotify connect is not working when the phoniebox is playing music. It cannot interrupt it. You need to pause the music first and then connect with spotify connect.
- When Spotify connect is used, the phoniebox is not able to play music from the local library. You need to disconnect from spotify connect first and then play the local music.

## Gatefold display

In addition one can use gatefold, to show the currently playing song via a web browser.  
https://gitlab.com/deimi/gatefold

### Webserver
```bash
# Setup env file according to the example in https://gitlab.com/deimi/gatefold/-/blob/main/webapp/.env.example
docker run -v /etc/gatefold/webapp_env:/app/.env -d --restart "on-failure" --name gatefold_webapp -p 80:8080 registry.gitlab.com/deimi/gatefold/webapp:latest
```

### Phoniebox

Install nvm https://github.com/nvm-sh/nvm#installing-and-updating

```bash
git clone https://gitlab.com/deimi/gatefold.git
cd gatefold/raspotify
./setup.sh
# It doesn't matter which github user you enter here because you can change the env file later anyway
sudo systemctl daemon-reload && sudo systemctl restart raspotify
```
