#!/usr/bin/env python3

import schemdraw
import schemdraw.elements as elm


class RpiHeader(schemdraw.elements.connectors.Header):
    default_pins_on_the_left = ['3V3', 'GPIO2', 'GPIO3', 'GPIO4', 'GND', 'GPIO17', 'GPIO27', 'GPIO22', '3V3',
                                'GPIO10', 'GPIO9', 'GPIO11', 'GND', 'ID_SD', 'GPIO5', 'GPIO6', 'GPIO13', 'GPIO19', 'GPIO26', 'GND']
    default_pins_on_the_right = ['5V', '5V', 'GND', 'GPIO14', 'GPIO15', 'GPIO18', 'GND', 'GPIO23', 'GPIO24',
                                 'GND', 'GPIO25', 'GPIO8', 'GPIO7', 'ID_SC', 'GND', 'GPIO12', 'GND', 'GPIO16', 'GPIO20', 'GPIO21']

    def __init__(self, pins_on_the_left=default_pins_on_the_left, pins_on_the_right=default_pins_on_the_right) -> None:
        super().__init__(shownumber=True, rows=20, cols=2,
                         pinsleft=pins_on_the_left, pinalignleft='top', pinsright=pins_on_the_right, pinalignright='top')


with schemdraw.Drawing(backend="svg") as d:
    d += (rpi_header := RpiHeader().label("RPI header"))

    # WM8960 audio hat connection
    d += elm.Line().left(d.unit/2).at(rpi_header.pin3).label("Audio_hat", "left")
    d += elm.Line().left(d.unit/2).at(rpi_header.pin5).label("Audio_hat", "left")
    d += elm.Line().left(d.unit/2).at(rpi_header.pin35).label("Audio_hat", "left")
    d += elm.Line().right(d.unit/2).at(rpi_header.pin12).label("Audio_hat", "right")
    d += elm.Line().right(d.unit/2).at(rpi_header.pin38).label("Audio_hat", "right")
    d += elm.Line().right(d.unit/2).at(rpi_header.pin40).label("Audio_hat", "right")

    # RFID reader
    d += (rfid_data := elm.Header(cols=1, rows=6, pinalignleft="center",
                                  pinsleft=["RST", "IRQ", "MISO", "MOSI", "SCK", "SDA"])
          .anchor("pin5")
          .at(rpi_header.pin23, dx=-12)
          .label("RFID data"))
    d += elm.Wire().at(rfid_data.pin5).to(rpi_header.pin23)
    d += elm.Wire("z", k=4).at(rfid_data.pin3).to(rpi_header.pin21)
    d += elm.Wire("z", k=4).at(rfid_data.pin4).to(rpi_header.pin19)
    d += elm.Line().right(d.unit/2).at(rfid_data.pin2).label("RFID_IRQ", "right")
    d += elm.Line().right(d.unit/2).at(rpi_header.pin18).label("RFID_IRQ", "right")
    d += elm.Line().right(d.unit/2).at(rfid_data.pin1).label("RFID_RST", "right")
    d += elm.Line().right(d.unit/2).at(rpi_header.pin22).label("RFID_RST", "right")
    d += elm.Line().right(d.unit/2).at(rfid_data.pin6).label("RFID_SDA", "right")
    d += elm.Line().right(d.unit/2).at(rpi_header.pin24).label("RFID_SDA", "right")
    d += (rfid_supply := elm.Header(cols=1, rows=2, pinalignleft="center", pinsleft=["3V3", "GND"])
          .anchor("pin2")
          .at(rpi_header.pin9, dx=-12)
          .label("RFID supply"))
    d += elm.Wire().at(rfid_supply.pin2).to(rpi_header.pin9)
    d += elm.Wire("c", k=3).at(rfid_supply.pin1).to(rpi_header.pin1)

    # OnOff Shim
    d += elm.Line().left(d.unit/2).at(rpi_header.pin11).label("/Shim_Status", "left")
    d += elm.Line().left(d.unit/2).at(rpi_header.pin7).label("/Shim_Off", "left")
    d += (onoff_btn := elm.Header(cols=1, rows=2)
          .anchor("pin1")
          .at(rpi_header.pin15, dx=-6)
          .label("OnOff btn"))
    d += elm.Line().right(d.unit/2).at(onoff_btn.pin1).label("Shim_btn", "right")
    d += elm.Line().right(d.unit/2).at(onoff_btn.pin2)

    # Buttons
    d += (play_btn := elm.Header(cols=1, rows=2, pinsleft=["--", "+"])
          .anchor("pin1")
          .at(rpi_header.pin9, dx=-6)
          .label("Play"))
    d += elm.Wire("c", k=1.3).at(play_btn.pin2).to(rpi_header.pin13)
    d += (next_btn := elm.Header(cols=1, rows=2, pinsleft=["+", "--"])
          .anchor("pin1")
          .at(rpi_header.pin29, dx=-6)
          .label("Next"))
    d += elm.Wire("|-").at(next_btn.pin1).to(rpi_header.pin29)
    d += (prev_btn := elm.Header(cols=1, rows=2, pinsleft=["--", "+"])
          .anchor("pin1")
          .at(rpi_header.pin35, dx=-6)
          .label("Prev"))
    d += elm.Wire("|-").at(prev_btn.pin2).to(rpi_header.pin37)
    d += (vol_up_btn := elm.Header(cols=1, rows=2, pinsleft=["+", "--"])
          .anchor("pin1")
          .at(rpi_header.pin29, dx=-12)
          .label("Vol up"))
    d += elm.Line().right(d.unit/2).at(vol_up_btn.pin1).label("Vol_up+", "right")
    d += elm.Line().left(d.unit/2).at(rpi_header.pin31).label("Vol_up+", "left")
    d += (vol_down_btn := elm.Header(cols=1, rows=2, pinsleft=["--", "+"])
          .anchor("pin1")
          .at(rpi_header.pin35, dx=-12)
          .label("Vol down"))
    d += elm.Line().right(d.unit/2).at(vol_down_btn.pin2).label("Vol_down+", "right")
    d += elm.Line().left(d.unit/2).at(rpi_header.pin33).label("Vol_down+", "left")
    d += elm.Wire("c", k=5).at(vol_down_btn.pin1).to(rpi_header.pin39)
    d += elm.Line().left(1).at(prev_btn.pin1).dot()
    d += elm.Wire("|-").to(next_btn.pin2)
    d += elm.Line().at(vol_up_btn.pin2).to(next_btn.pin2, dx=-1).dot()

    # LED
    d += (led1 := elm.Header(cols=1, rows=2, pinsleft=["--", "+"])
          .anchor("pin1")
          .at(rpi_header.pin6, dx=6)
          .label("LED_Volup"))
    d += elm.OrthoLines(n=2).at(rpi_header.pin6).to(led1.pin1)
    d += (led2 := elm.Header(cols=1, rows=2, pinsleft=["--", "+"])
          .anchor("pin1")
          .at(rpi_header.pin14, dx=6)
          .label("LED_Voldown"))
    d += elm.OrthoLines(n=2).at(rpi_header.pin14).to(led2.pin1)
    d += (led3 := elm.Header(cols=1, rows=2, pinsleft=["--", "+"])
          .anchor("pin1")
          .at(rpi_header.pin20, dx=6)
          .label("LED_Prev"))
    d += elm.Wire("-").at(rpi_header.pin20).to(led3.pin1)
    d += elm.Wire("c", k=4).at(rpi_header.pin26).to(led3.pin2)
    d += (led4 := elm.Header(cols=1, rows=2, pinsleft=["--", "+"])
          .anchor("pin1")
          .at(rpi_header.pin28, dx=6)
          .label("LED_Play"))
    d += elm.OrthoLines(n=2).at(rpi_header.pin30).to(led4.pin1)
    d += (led5 := elm.Header(cols=1, rows=2, pinsleft=["--", "+"])
          .anchor("pin1")
          .at(rpi_header.pin34, dx=6)
          .label("LED_Next"))
    d += elm.OrthoLines(n=2).at(rpi_header.pin34).to(led5.pin1)

    d.save("rpi_header_connection.svg", transparent=False)
