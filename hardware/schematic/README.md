![Schematic](rpi_header_connection.svg)

On one hand this schematic shows to connection to the RPi header. On the other hand it is also a rough placement of the parts and wires on the actual board.

# Plot schematic
I used [schemdraw](https://schemdraw.readthedocs.io) for writing the schematic as code and plot it as well.

```bash
pip install -r python_requirements.txt
./rpi_header_connection.py
```
