
module __Customizer_Limit__()
{
}

draw_line_thickness = 0.1;

module drill_hole(d)
{
	difference()
	{
		circle(d = d);
		circle(d = d - draw_line_thickness);
	}
	square([ draw_line_thickness, 3 ], center = true);
	square([ 3, draw_line_thickness ], center = true);
}

module box(width, height)
{
	difference()
	{
		square([ width, height ], center = true);
		square([ width - draw_line_thickness, height - draw_line_thickness ], center = true);
	};
}

module sound_control()
{
	drill_hole(d = 33);
}

module all_sound_controls()
{
	sound_control_allignment_vert = 40;
	sound_control_allignment_hori = 40;
	module two_sound_controls()
	{
		translate([ sound_control_allignment_hori, 0, 0 ])
		{
			sound_control();
			translate([ 0, sound_control_allignment_vert, 0 ])
			sound_control();
		}
	}
	mirror([ 1, 0, 0 ])
	{
		two_sound_controls();
		mirror([ 0, 1, 0 ]) two_sound_controls();
	}
	translate([ 0, sound_control_allignment_vert, 0 ])
	sound_control();
	translate([ sound_control_allignment_hori, sound_control_allignment_vert, 0 ])
	sound_control();
}

module on_off()
{
	translate([ 125, 53, 0 ])
	drill_hole(d = 11);
}

module all_speaker()
{
	module speaker()
	{
		box(45, 100);
		module sound_holes_quarter()
		{
			diameter = 4;
			offset = diameter + 2.5;
			translate([ offset * 0, offset * 0, 0 ])
			drill_hole(diameter);
			translate([ offset * 1, offset * 0, 0 ])
			drill_hole(diameter);
			translate([ offset * 2, offset * 0, 0 ])
			drill_hole(diameter);
			translate([ offset * 0, offset * 1, 0 ])
			drill_hole(diameter);
			translate([ offset * 1, offset * 1, 0 ])
			drill_hole(diameter);
			translate([ offset * 2, offset * 1, 0 ])
			drill_hole(diameter);
			translate([ offset * 0, offset * 2, 0 ])
			drill_hole(diameter);
			translate([ offset * 1, offset * 2, 0 ])
			drill_hole(diameter);
		}
		module sound_holes()
		{
			sound_holes_quarter();
			mirror([ 1, 0, 0 ]) sound_holes_quarter();
			mirror([ 1, 1, 0 ]) sound_holes_quarter();
			mirror([ 0, 1, 0 ]) sound_holes_quarter();
		}
		translate([ 0, 22, 0 ])
		sound_holes();
		translate([ 0, -22, 0 ])
		sound_holes();
	}
	translate([ 95, 8, 0 ])
	speaker();
}

module card_reader()
{
	translate([ 25, -20, 0 ])
	union()
	{
		box(85, 55);
		rotate(a = 45)
		{
			square([ 0.3, 70 ], center = true);
			square([ 70, 0.3 ], center = true);
		}
	}
}

module border()
{
	module border()
	{
		width = 200;
		height = 125;
		overlap = 5;
		box(width, height);
		box(width + overlap * 2, height + overlap * 2);
	}
	translate([ 35, 0, 0 ])
	border();
}

all_sound_controls();
on_off();
all_speaker();
card_reader();
border();
